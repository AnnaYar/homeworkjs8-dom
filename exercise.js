// Теоретичні питання

// 1. Опишіть своїми словами що таке Document Object Model (DOM).

// DOM – об'єктна модель документу, за допомогою якої все, що міститься на сторінці, представлене
//  у вигляді об'єктів, які можна змінювати. Кожен тег утворює окремий елемент-вузол,
// кожен фрагмент тексту – текстовий елемент.
//   HTML - документ це ієрархічне дерево, у якому кожен елемент(крім кореневого) має лише одного
// батька, тобто елемент, у якому він розташовується. Це дерево утворюється за рахунок вкладеної
// структури тегів та текстових елементів.
// За допомогою набору властивостей та методів можливо шукати, створювати та видаляти елементи.

// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

// innerHTML - дає можливість отримати або встановити HTML - вміст елементу, включаючи його теги
// та текст.
// innerText - дає можливість отримати або встановити тільки текстовий вміст елементу, він не
// інтерпритує HTML -розмітку.

// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

// document.getElementById(id) - якщо елемент має атрибут id.

// elem.querySelector(css) - повертає перший елемент, що відповідає даному CSS - селектору.
  
// elem.querySelectorAll(css) - найбільш гнучкий метод, який повертає всі елементи всередині elem,
//   що відповідають заданому CSS - селектору.


//     Завдання

/*
1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000

2. Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та 
вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

3. Встановіть в якості контента елемента з класом testParagraph наступний параграф -

This is a paragraph

4. Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. 
Кожному з елементів присвоїти новий клас nav-item.

5.Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.*/


const paraPage = document.querySelectorAll('p');
for (let para of paraPage) {
  para.style.backgroundColor = "#ff0000"
}

const idElem = document.getElementById('optionsList');
console.log('Елемент із id="optionsList":',idElem);

const idElemParent = idElem.parentNode;
console.log('Батьківський елемент:', idElemParent);

const idElemChildren = idElem.childNodes;
console.log('Дочірні ноди:', idElemChildren);

for (let child of idElemChildren) {
  console.log('Назва дочірнього ноду:', child.nodeName, 'Тип:', child.nodeType);
}

const newPara = document.getElementById('testParagraph');
newPara.textContent ='This is a paragraph';

const mainHeader = document.querySelector('.main-header');
const mainHeaderElem = mainHeader.children;
console.log('Елементи, вкладені в елемент із класом main-header:', mainHeaderElem);
for (const elem of mainHeaderElem) {
  elem.classList.replace('container', 'nav-item');
}

const sectionTitleElem = document.querySelectorAll('.section-title');
for (const element of sectionTitleElem) {
  element.classList.remove('section-title');
}